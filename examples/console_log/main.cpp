#include <QCoreApplication>
#include "csv_log.h"
#include "memory"
#include <QDebug>

#define TIME_CLMN_NAME "Time"
#define VOLTAGE_CLMN_NAME "Voltage"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    qInfo()<<"Create object";
    csv_log mlog;
    std::shared_ptr<csv_column> time_clmn = mlog.addColumn(TIME_CLMN_NAME);
    std::shared_ptr<csv_column> vol_clmn = mlog.addColumn(VOLTAGE_CLMN_NAME);

    long double time{1.0L}, volt{5.5L};
    qInfo()<<"Set [Time] column val"<<static_cast<double>(time);
    time_clmn->setValue(time);
    qInfo()<<"Set [Voltage] column val"<<static_cast<double>(time);
    vol_clmn->setValue(volt);
    qInfo()<<"Close CSV line, write data to log file";
    mlog.endl();

    time = 2.0L;
    qInfo()<<"set [Time] column val"<<static_cast<double>(time);
    time_clmn->setValue(time);

    qInfo()<<"no [Voltage] column val - n/a";
    //  vol_clmn->setValue(3.3L); // commented to write "n/a" val;

    qInfo()<<"Close CSV line, write data to log file";
    mlog.endl();

    std::shared_ptr<csv_column> t_clmn= mlog.column(TIME_CLMN_NAME);

    qInfo()<<t_clmn->getHeader()<<t_clmn->getValue();



    qInfo()<<"Close log file";
    mlog.closeSession();

    // return a.exec();
}
