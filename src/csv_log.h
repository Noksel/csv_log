#ifndef CSV_LOG_H
#define CSV_LOG_H

#include <QObject>
#include <QMap>
#include <QDir>
#include <QTextStream>
#include <QDateTime>
#include <memory>
#include "csv_column.h"

#define DEFAULT_LOG_DIR "./log"

//using namespace std;

class csv_log : public QObject
{
    Q_OBJECT
private:
    QMap<QString,std::shared_ptr<csv_column>> m_clumns;
    QStringList m_keys;
    QString m_delimiter;
    QString m_prefix;

    std::shared_ptr<QFile> m_file;
    std::unique_ptr<QTextStream> m_stream;
    bool m_inited;

public:
    explicit csv_log(QObject *parent = nullptr);
    explicit csv_log(QString prefix, QObject *parent = nullptr);
    ~csv_log() override;
    std::shared_ptr<csv_column> addColumn(QString Header);
    bool createLogFile();
    void endl();
    void closeSession();
    std::shared_ptr<csv_column> column(QString Header);



signals:

};

#endif // CSV_LOG_H
