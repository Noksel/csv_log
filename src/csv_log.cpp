#include "csv_log.h"

csv_log::csv_log(QObject *parent)
    : QObject{parent},m_delimiter(";"),m_prefix(""),m_inited(false)
{
    QDir clbr_dir(DEFAULT_LOG_DIR);
    if (!clbr_dir.exists())
    {
        QDir mkd("./");
        mkd.mkdir(DEFAULT_LOG_DIR);
    }
}

csv_log::csv_log(QString prefix, QObject *parent)
: QObject{parent},m_delimiter(";"),m_prefix(prefix+"_"),m_inited(false)
{
    QDir clbr_dir(DEFAULT_LOG_DIR);
    if (!clbr_dir.exists())
    {
        QDir mkd("./");
        mkd.mkdir(DEFAULT_LOG_DIR);
    }

}

csv_log::~csv_log()
{
    closeSession();
}

std::shared_ptr<csv_column> csv_log::addColumn(QString Header)
{
    std::shared_ptr<csv_column> column = std::make_shared<csv_column>(Header);
    m_clumns.insert(Header,column);
    m_keys.append(Header);
    return column;
}

void csv_log::endl()
{
    if (!m_inited)
        createLogFile();

    QStringList lst;
//    for (shared_ptr<csv_column> clmn: qAsConst(m_clumns)){
//        lst.append(clmn->getValue());
//    }
    for (const QString &key: qAsConst(m_keys)){
        lst.append(m_clumns[key]->getValue());
    }

    (*m_stream)<<lst.join(m_delimiter);
    (*m_stream)<< "\n";
    m_stream->flush();
}

void csv_log::closeSession()
{
    m_inited=false;
    if (m_stream)
        m_stream->flush();
    if (m_file&&m_file->isOpen())
        m_file->close();
}

std::shared_ptr<csv_column> csv_log::column(QString Header)
{
    return m_clumns.value(Header);
}

bool csv_log::createLogFile()
{
    m_file.reset(new QFile("./log/"+m_prefix+(QDateTime::currentDateTime().toString("yyyy_MM_dd_hhmmss"))+QString(".csv")));
    m_file->setPermissions(QFileDevice::WriteOther);
    if (!m_file->open(QIODevice::WriteOnly|QIODevice::Text))
        return false;
    m_stream.reset(new QTextStream(m_file.get()));

//    QStringList lst;
//    for (shared_ptr<csv_column> clmn: qAsConst(m_clumns)){
//        lst.append(clmn->getHeader());
//    }

    (*m_stream)<<m_keys.join(m_delimiter);
    (*m_stream)<< "\n";
    m_stream->flush();
    m_inited=true;
    return true;
}
