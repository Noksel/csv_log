#ifndef CSV_COLUMN_H
#define CSV_COLUMN_H

#include <QObject>

class csv_column : public QObject
{
    Q_OBJECT


private:
    QString m_header;
  //  long double m_data;
    QString m_str_value;

public:
    explicit csv_column(QString header,QObject *parent = nullptr);
    void setValue(long double value);
    void setValue(uint64_t value);
    void setValue(uint val);
    QString getHeader();
    QString getValue();
    void setHeader(QString header);


signals:

};

#endif // CSV_COLUMN_H
