#include "csv_column.h"

csv_column::csv_column(QString header, QObject *parent)
    : QObject{parent}, m_header(header),m_str_value("n/a")
{

}

void csv_column::setValue(long double value)
{
    m_str_value = QString::number(value,'g').replace(".",",");
}

void csv_column::setValue(uint64_t value)
{
    m_str_value = QString::number(value);
}

void csv_column::setValue(uint val)
{
    setValue(static_cast<uint64_t>(val));
}

QString csv_column::getHeader()
{
    return m_header;
}

QString csv_column::getValue()
{
    QString tmp = m_str_value;
    m_str_value="n/a";
    return tmp;
}

void csv_column::setHeader(QString header)
{
    m_header=header;
}
